#!/bin/bash

PACKAGE="$1"

cd /tmp/setup

wget https://bitbucket.org/sseidenthal/server-setup-tool/raw/master/scripts/$PACKAGE.sh
chmod +x $PACKAGE.sh
echo "Start installing $PACKAGE";
sleep 5;
./$PACKAGE.sh