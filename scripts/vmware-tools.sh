#!/bin/bash

clear;

echo "** Please make sure you have inserted VMWare Tools CD ! **"
echo "(Menu > Virtual Machine > Install VMWare Tools )"
read -p "To continue: (Y)" -n 1 -r
echo    # (optional) move to a new line$

if [[ $REPLY =~ ^[Yy]$ ]]
then

	mount /dev/cdrom /media/cdrom
	tar -xzvf /media/cdrom/VMwareTools-* -C /tmp/
	cd /tmp/vmware-tools-distrib/

	#install build essentials
	sudo apt-get install build-essential -y

	#install linux headers
	sudo apt-get install linux-headers-$(uname -r) -y

	sudo ./vmware-install.pl -d

fi

