#!/bin/bash

apt-get -y update
apt-get -y install php5 php5-dev php5-cli php-pear php5-mcrypt

a2enmod rewrite

service apache2 restart