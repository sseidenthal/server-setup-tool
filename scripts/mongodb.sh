#!/bin/bash

echo "deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen" | tee -a /etc/apt/sources.list.d/10gen.list

apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10

apt-get -y update
apt-get -y install mongodb-10gen