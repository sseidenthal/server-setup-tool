#!/bin/bash

export LANGUAGE=en_US.UTF-8
export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8
locale-gen en_US.UTF-8
dpkg-reconfigure locales

# update installed
apt-get update
#apt-get dist-upgrade -y
apt-get install dialog sudo curl mc -y

# open fd
exec 3>&1
 
# Store data to $VALUES variable
VALUES=$(dialog --ok-label "Install" \
	  --separate-output \
	  --backtitle "" \
	  --clear \
	  --title "Setup your server" \
	  --checklist "Choose packages to install" \
15 70 0 \
	apache2php5 "Apache2 with PHP5" off \
	mongodb "mongodb server" off \
	git "git client" off \
	composer "Composer" off \
	vmware-tools "VMware Tools" off \
	vbox-tools "VBox Tools" off \
2>&1 1>&3)

RESULT=$?

# close fd
exec 3>&-

if [ $RESULT -neq 0 ] ; then
	exit
fi

# prepare temp folder
mkdir -p /tmp/setup
cd /tmp/setup
rm -Rf *

# get proxy setup file
wget https://bitbucket.org/sseidenthal/server-setup-tool/raw/master/scripts/_setup.sh
chmod +x _setup.sh

# display values just entered
for PACKAGE in $VALUES
do
	./_setup.sh $PACKAGE
done

#Cleaning packages
apt-get autoremove -y
